# Python for Qlik Deployment Framework Projects


## Get Started

#### Repository
Class to represent the project repository in Qlik Deployment Framework.



#### Container
Class to represent each container in Qlik Deployment Framework.

For default the pattern of container is base on 'Qlik Sense Only'


#### QVS
Class to represent each qvs file inside of container in Qlik Deployment Framework.

For default the pattern of container is base on 'Qlik Sense Only'


#### QVF

Class to represent each qvf application inside of container in Qlik Deployment Framework.


## License
PYQDF as a whole is licensed under the GNU Public License, Version 3. Individual files may have a different, but compatible license.

See LICENSE for details.